package com.mobntic.catalog.service;

import com.mobntic.catalog.config.CatalogAutoConfig;
import com.mobntic.catalog.config.CatalogConfig;
import com.mobntic.catalog.repository.document.CatalogDocument;
import com.mobntic.catalog.repository.document.NumberFacetData;
import com.mobntic.catalog.repository.document.ProductItem;
import com.mobntic.catalog.repository.document.StringFacetData;
import com.mobntic.catalog.repository.query.CatalogSearchQuery;
import com.mobntic.catalog.repository.reponse.CatalogSearchQueryResponse;
import com.mobntic.catalog.repository.reponse.CatalogSuggestCompletionResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.mobntic.catalog.tools.DocumentBuilderTools.builCatalogDocument;
import static com.mobntic.catalog.tools.DocumentBuilderTools.buildCatalogDocument;
import static com.mobntic.catalog.tools.DocumentBuilderTools.buildCategory;
import static com.mobntic.catalog.tools.DocumentBuilderTools.buildCategoryScores;
import static com.mobntic.catalog.tools.DocumentBuilderTools.buildProductItem;
import static com.mobntic.catalog.tools.DocumentBuilderTools.buildScores;
import static com.mobntic.catalog.tools.DocumentBuilderTools.buildSearchData;
import static com.mobntic.catalog.tools.DocumentBuilderTools.buildSkuItem;
import static com.mobntic.catalog.tools.DocumentBuilderTools.extractCompletionTerms;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {CatalogConfig.class, CatalogAutoConfig.class})
@ActiveProfiles("test")
public class DocumentServiceTest {

    @Autowired
    private DocumentService documentService;

    @Autowired
    private ElasticsearchOperations elasticsearchTemplate;

    @Before
    public void before() {
        elasticsearchTemplate.deleteIndex(CatalogDocument.class);
        elasticsearchTemplate.createIndex(CatalogDocument.class);
        elasticsearchTemplate.putMapping(CatalogDocument.class);
        elasticsearchTemplate.refresh(CatalogDocument.class);
    }

    @Test
    public void findBySkuId_should_return_the_corresponding_item() throws Exception {

        CatalogDocument document = initLoopProducts(1).iterator().next();

        CatalogDocument documentFound = documentService.findById("productId1").orElseThrow(Exception::new);

        assertNotNull(documentFound.getProductItem().getId());
        assertEquals(documentFound.getProductItem().getName(), document.getProductItem().getName());
        assertEquals(documentFound.getProductItem().getDescription(), document.getProductItem().getDescription());
        assertEquals(documentFound.getProductItem().getPrice(), document.getProductItem().getPrice());

    }

    @Test
    public void findBySkuName_should_return_the_corresponding_item() throws Exception {

        CatalogDocument document = initLoopProducts(1).iterator().next();

        Page<CatalogDocument> pageFound = documentService.findByName("name1", PageRequest.of(0, 10));

        CatalogDocument documentFound = pageFound.getContent().iterator().next();

        assertNotNull(documentFound.getProductItem().getId());
        assertEquals(documentFound.getProductItem().getName(), document.getProductItem().getName());
        assertEquals(documentFound.getProductItem().getDescription(), document.getProductItem().getDescription());
        assertEquals(documentFound.getProductItem().getPrice(), document.getProductItem().getPrice());

    }

    @Test
    public void findByPriceBetween_should_return_only_items_in_price_interval() throws Exception {

        initLoopProducts(4);

        List<ProductItem> result = documentService.findByPriceBetween(12d, 24d);

        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(2, result.size());

    }

    @Test
    public void findByQuery_should_return_only_items_matching_query_and_facet_filter_and_all_related_facets()
            throws Exception {

        initLoopProducts(4);

        Map<String, String[]> stringFacetFilters = new HashMap<>();
        stringFacetFilters.put("manufacturer", new String[]{"Dell_name1_1"});

        CatalogSearchQueryResponse response = documentService.findAndGroupByQuery(CatalogSearchQuery.builder()
                .page(0)
                .pageSize(10)
                .stringFacets(stringFacetFilters)
                .build());

        assertEquals(1, response.getTotalElements());

        assertFalse(response.getStringFacets().isEmpty());
        assertFalse(response.getNumericFacets().isEmpty());

        assertEquals(2, response.getStringFacets().size());
        assertEquals(1, response.getNumericFacets().size());

        assertEquals(4, response.getStringFacets().get("format").size());
        assertEquals(4, response.getStringFacets().get("manufacturer").size());
        assertEquals(1, response.getNumericFacets().get("final_gross_price").size());

    }

    @Test
    public void findByQuery_should_return_items_matching_query_with_related_facets() throws Exception {

        initLoopProducts(4);

        CatalogSearchQueryResponse response = documentService.findAndGroupByQuery(CatalogSearchQuery.builder()
                .page(0)
                .pageSize(10)
                .build());

        assertNotNull(response);
        assertEquals(4, response.getTotalElements());
        assertFalse(response.getStringFacets().isEmpty());
        assertFalse(response.getNumericFacets().isEmpty());

        assertEquals(2, response.getStringFacets().size());
        assertEquals(1, response.getNumericFacets().size());

        assertEquals(4, response.getStringFacets().get("format").size());
        assertEquals(4, response.getStringFacets().get("manufacturer").size());
        assertEquals(1, response.getNumericFacets().get("final_gross_price").size());

    }

    @Test
    public void findByQuery_should_return_autocompletion_suggestion() throws Exception {

        initMockProducts();

        CatalogSuggestCompletionResponse completionResponse = documentService
                .suggestCompletion(CatalogSearchQuery.builder().query("Int").build());

        assertEquals(1, completionResponse.getManufacturerFacetValue().size());
        assertEquals(2, completionResponse.getProducts().size());
        assertTrue(completionResponse.getCategories().isEmpty());

    }

    @Test
    public void findByQuery_should_return_autocompletion_suggestion_2() throws Exception {

        initMockProducts();

        CatalogSuggestCompletionResponse completionResponse = documentService
                .suggestCompletion(CatalogSearchQuery.builder().query("Imp").build());

        assertEquals(1, completionResponse.getCategories().size());
        assertEquals(1, completionResponse.getProducts().size());
        assertTrue(completionResponse.getManufacturerFacetValue().isEmpty());

    }

    @Test
    public void findByQuery_should_return_items_matching_with_query() throws Exception {

        initMockProducts();

        CatalogSearchQueryResponse response = documentService.findAndGroupByQuery(CatalogSearchQuery.builder()
                .page(0)
                .query("Intel")
                .pageSize(10)
                .build());

        assertNotNull(response);
        assertEquals(2, response.getTotalElements());
        assertFalse(response.getStringFacets().isEmpty());
        assertFalse(response.getNumericFacets().isEmpty());

    }

    private List<CatalogDocument> initLoopProducts(int documentCount) {
        List<CatalogDocument> data = new ArrayList<>();
        for (int i = 1;
             i <= documentCount;
             i++) {
            data.add(addCatalogDocument("productId" + i, "skuId" + i, "name" + i, "description+" + i, 12d * i, Long.valueOf(i),
                    "category" + i));
        }
        return data;
    }

    private void initMockProducts() {
        addCatalogDocument(addPrinter());
        addCatalogDocument(addProcessor1());
        addCatalogDocument(addProcessor2());
    }

    private CatalogDocument addProcessor2() {
        return buildCatalogDocument("pdt3",
                buildProductItem(
                        "pdt3",
                        "Intel Core i7-6700K Box 6 Gen",
                        "La puissance dans sa nature...",
                        3700d,
                        asList(
                                buildSkuItem(
                                        "pdt3_sku1",
                                        "Intel Core i7-6700K Box 6 Gen",
                                        51500d,
                                        "image_key"
                                ).build()
                        ),
                        "image_key",
                        "brand"
                ).build(),

                buildSearchData("pdt2_sku1, Processur, Intel Core i7-6700K Box 6 Gen, i7",
                        "Je sais pas encore quoi mettre là",
                        asList(
                                new StringFacetData("Marque", "Intel"),
                                new StringFacetData("Type", "i7")
                        ),
                        asList(
                                new NumberFacetData("Prix", 51500d),
                                new NumberFacetData("Coeurs", 4d),
                                new NumberFacetData("Fréquence", 4.2d),
                                new NumberFacetData("Mémoire", 8d),
                                new NumberFacetData("Garentie", 3d)
                        )
                ).build(),
                extractCompletionTerms(
                        asList("Processeurs"),
                        asList("Intel", "Core", "i7-6700K", "i7", "6700K", "Box", "Gen"),
                        asList("Intel"),
                        asList("Shop", "Shop&TIC", "TIC")
                ).build(),
                buildScores().build(),
                buildCategory(10l, "Processeurs").build(),
                buildCategoryScores().build()
        ).build();
    }

    private CatalogDocument addProcessor1() {
        return buildCatalogDocument("pdt2",
                buildProductItem(
                        "pdt2",
                        "Intel Celeron G1840 Tray",
                        "Un des meilleur rapport qualité prix",
                        3700d,
                        asList(
                                buildSkuItem(
                                        "pdt2_sku1",
                                        "Intel Celeron G1840 Tray",
                                        3700d,
                                        "image_key"
                                ).build()
                        ),
                        "image_key",
                        "brand"
                ).build(),
                buildSearchData("pdt2_sku1, Processur, Intel Celeron G1840 Tray",
                        "Je sais pas encore quoi mettre là",
                        asList(
                                new StringFacetData("Marque", "Intel"),
                                new StringFacetData("Type", "Celeron")
                        ),
                        asList(
                                new NumberFacetData("Prix", 3700d),
                                new NumberFacetData("Coeurs", 2d),
                                new NumberFacetData("Fréquence", 2.8d),
                                new NumberFacetData("Mémoire", 2d),
                                new NumberFacetData("Garentie", 3d)
                        )
                ).build(),
                extractCompletionTerms(
                        asList("Processeurs"),
                        asList("Intel", "Celeron", "G1840", "Tray"),
                        asList("Intel"),
                        asList("Shop", "Shop&TIC", "TIC")
                ).build(),
                buildScores().build(),
                buildCategory(10l, "Processeurs").build(),
                buildCategoryScores().build()
        ).build();
    }

    private CatalogDocument addPrinter() {
        return buildCatalogDocument("pdt1",
                buildProductItem(
                        "pdt1",
                        "Imprimante Jet d'ancre",
                        "La meeilleur de toutes les imprimantes",
                        15000d,
                        asList(
                                buildSkuItem(
                                        "pdt1_sku1",
                                        "Imprimante Jet d'ancre",
                                        15000d,
                                        "image_key"
                                ).build()
                        ),
                        "image_key",
                        "brand"
                ).build(),
                buildSearchData(
                        "pdt1_sku1, Imprimante Jet d'ancre, La description de l'imprimante, Impriamntes",
                        "Je sais pas encore quoi mettre là",
                        asList(
                                new StringFacetData("Marque", "HP"),
                                new StringFacetData("Cartouche", "Jet d'ancre"),
                                new StringFacetData("Format", "14"),
                                new StringFacetData("Type", "Couleur")
                        ),
                        asList(
                                new NumberFacetData("Prix", 15000d),
                                new NumberFacetData("Garentie", 3d)
                        )
                ).build(),
                extractCompletionTerms(
                        asList("Imprimante", "Jet", "Ancre", "d'ancre"),
                        asList("Imprimante", "Jet", "Ancre", "d'ancre"),
                        asList("HP"),
                        asList("Shop", "Shop&TIC", "TIC")
                ).build(),
                buildScores().build(),
                buildCategory(30l, "Imprimantes").build(),
                buildCategoryScores().build()
        ).build();
    }

    private CatalogDocument addCatalogDocument(String productId, String skuId, String name, String desc, Double price) {
        return addCatalogDocument(productId, skuId, name, desc, price, 1l, "category");
    }

    private CatalogDocument addCatalogDocument(String productId, String skuId, String name, String desc, Double price, Long categoryId,
                                               String categoryName) {
        return addCatalogDocument(builCatalogDocument(productId, skuId, name, desc, price, categoryId, categoryName).build());
    }

    private CatalogDocument addCatalogDocument(CatalogDocument documentBuilder) {
        return documentService.save(documentBuilder);
    }

}
