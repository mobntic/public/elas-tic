package com.mobntic.catalog.config;

import lombok.extern.log4j.Log4j2;
import lombok.extern.slf4j.Slf4j;
import org.apache.logging.log4j.util.Strings;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import pl.allegro.tech.embeddedelasticsearch.EmbeddedElastic;
import pl.allegro.tech.embeddedelasticsearch.PopularProperties;

import java.io.IOException;
import java.net.InetAddress;

import static org.apache.logging.log4j.util.Strings.isNotBlank;

@Log4j2
@Configuration
@ConditionalOnClass(ElasticsearchTemplate.class)
@EnableConfigurationProperties(CatalogConfig.class)
@EnableElasticsearchRepositories(basePackages = "com.mobntic.catalog.repository")
@ComponentScan("com.mobntic.catalog")
public class CatalogAutoConfig {

    private static final String CATALOG_TRANSPORT_PORT = "com.mobntic.catalog.elasticsearch.transport_port";
    private static final String CATALOG_HOST = "com.mobntic.catalog.elasticsearch.host";
    private static final String CATALOG_CLUSTER_NAME = "com.mobntic.catalog.elasticsearch.cluster_name";
    private static final String CATALOG_INDEX_NAME = "com.mobntic.catalog.elasticsearch.index_name";

    private static final int DEFAULT_TRANSPORT_PORT_VALUE = 9300;
    private static final boolean DEFAULT_EMBEDDED_VALUE = true;
    private static final String DEFAULT_HOSTNAME_VALUE = "localhost";
    private static final String DEFAULT_CLUSTER_NAME_VALUE = "mobntic_cluster";
    private static final String DEFAULT_INDEX_NAME_VALUE = "mobntic_catalog_index";

    private CatalogConfig catalogConfig;

    @Autowired
    void setCatalogConfig(CatalogConfig catalogConfig) {
        this.catalogConfig = catalogConfig;
    }

    @Bean
    @ConditionalOnMissingBean
    public CatalogConfig catalogConfig() {

        CatalogConfig config = new CatalogConfig();

        config.setEmbedded(DEFAULT_EMBEDDED_VALUE);

        if (catalogConfig.getTransportPort() <= 0) {
            int port = DEFAULT_TRANSPORT_PORT_VALUE;
            if (isNotBlank(System.getProperty(CATALOG_TRANSPORT_PORT))) {
                try {
                    port = Integer.getInteger(System.getProperty(CATALOG_TRANSPORT_PORT));
                } catch (Exception e) {
                    log.warn("Given catalog.port is not number");
                }
            }
            config.setTransportPort(port);
        }

        config.setHost(getDefaultValue(catalogConfig.getHost(), CATALOG_HOST, DEFAULT_HOSTNAME_VALUE));

        config.setClusterName(
                getDefaultValue(catalogConfig.getClusterName(), CATALOG_CLUSTER_NAME, DEFAULT_CLUSTER_NAME_VALUE));

        config.setIndexName(
                getDefaultValue(catalogConfig.getIndexName(), CATALOG_INDEX_NAME, DEFAULT_INDEX_NAME_VALUE));

        return catalogConfig;
    }

    private String getDefaultValue(String value, String defaultKey, String defaultValue) {
        if (Strings.isBlank(value)) {
            if (isNotBlank(System.getProperty(defaultKey))) {
                return System.getProperty(defaultKey);
            }
            return defaultValue;
        }
        return value;
    }

    @Bean
    @ConditionalOnMissingBean
    public TransportClient client(EmbeddedElastic embeddedElastic) throws IOException, InterruptedException {

        if (catalogConfig.isEmbedded()) {
            embeddedElastic.start();
        }

        TransportClient client = new PreBuiltTransportClient(getSettingsBuilder());

        client.addTransportAddress(new TransportAddress(InetAddress.getByName(catalogConfig.getHost()),
                catalogConfig.getTransportPort()));

        return client;
    }

    @Bean
    @ConditionalOnMissingBean
    public EmbeddedElastic embeddedElasticServer() {
        return EmbeddedElastic.builder().withElasticVersion(catalogConfig.getEmbeddedVersion())
                .withSetting(PopularProperties.HTTP_PORT, catalogConfig.getHttpPort())
                .withSetting(PopularProperties.TRANSPORT_TCP_PORT, catalogConfig.getTransportPort())
                .withSetting(PopularProperties.CLUSTER_NAME, catalogConfig.getClusterName()).build();
    }

    private Settings getSettingsBuilder() {
        Settings.Builder settings = Settings.builder();
        settings.put("cluster.name", catalogConfig.getClusterName());
        return settings.build();
    }

    @Bean
    @ConditionalOnMissingBean
    public ElasticsearchTemplate elasticsearchTemplate(TransportClient client) {
        return new ElasticsearchTemplate(client);
    }

}