package com.mobntic.catalog.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ConfigurationProperties("com.mobntic.catalog.elasticsearch")
public class CatalogConfig {

    private boolean embedded = true;
    private String embeddedVersion = "5.6.5";
    private String host = "localhost";
    private int httpPort = 9200;
    private int transportPort = 9300;
    private String clusterName = "mobntic_cluster";
    private String indexName = "catalog_index";
}
