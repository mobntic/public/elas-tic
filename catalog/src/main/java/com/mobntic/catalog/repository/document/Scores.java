package com.mobntic.catalog.repository.document;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import lombok.Builder;
import lombok.Getter;

@Document(indexName = "#{ catalogConfig.indexName }", type = "Scores")
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Scores {

    @Field(type = FieldType.Double)
    private Double topSeller = 1d;

    @Field(type = FieldType.Double)
    private Double pdpImpressions = 1d;

    @Field(type = FieldType.Double)
    private Double saleImpressionsRate = 1d;

    @Field(type = FieldType.Double)
    private Double dataQuality = 1d;

    @Field(type = FieldType.Double)
    private Double deliverySpeed = 1d;

    @Field(type = FieldType.Double)
    private Double random = 1d;

    @Field(type = FieldType.Double)
    private Double stock = 1d;

}