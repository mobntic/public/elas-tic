package com.mobntic.catalog.repository.reponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.suggest.completion.CompletionSuggestion;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.util.CollectionUtils;

import lombok.Getter;

import com.mobntic.catalog.repository.document.ProductItem;

@Getter
public class CatalogSuggestCompletionResponse {

    private List<String> categories;
    private List<ProductItem> products;
    private List<String> manufacturerFacetValue;

    public CatalogSuggestCompletionResponse() {
        this.categories = new ArrayList<>();
        this.products = new ArrayList<>();
        this.manufacturerFacetValue = new ArrayList<>();
    }

    public static CatalogSuggestCompletionResponse wrap(SearchResponse searchResponse) {
        CatalogSuggestCompletionResponse response = new CatalogSuggestCompletionResponse();

        response.wrapCategories(searchResponse.getSuggest().getSuggestion("completion-category").getEntries());
        response.wrapManufacturerFacetValues(
                searchResponse.getSuggest().getSuggestion("completion-manufacturer").getEntries());
        response.wrapProducts(searchResponse.getSuggest().getSuggestion("completion-product").getEntries());

        return response;
    }

    private void wrapCategories(List<?> completionCategories) {
        this.categories = new ArrayList<>();
        if (!CollectionUtils.isEmpty(completionCategories)) {
            CompletionSuggestion.Entry suggestion = (CompletionSuggestion.Entry) completionCategories.iterator().next();
            this.categories.addAll(extractCompletionValues(suggestion));
        }

    }

    private void wrapManufacturerFacetValues(List<?> facetValues) {
        this.manufacturerFacetValue = new ArrayList<>();
        if (!CollectionUtils.isEmpty(facetValues)) {
            CompletionSuggestion.Entry suggestion = (CompletionSuggestion.Entry) facetValues.iterator().next();
            this.manufacturerFacetValue.addAll(extractCompletionValues(suggestion));
        }

    }

    private void wrapProducts(List<?> products) {
        this.products = new ArrayList<>();
        if (!CollectionUtils.isEmpty(products)) {
            CompletionSuggestion.Entry suggestion = (CompletionSuggestion.Entry) products.iterator().next();
            this.products.addAll(extractCompletionProducts(suggestion));
        }

    }

    private Set<String> extractCompletionValues(CompletionSuggestion.Entry suggestion) {
        Set<String> values = new HashSet<>();
        if (!CollectionUtils.isEmpty(suggestion.getOptions())) {
            for (CompletionSuggestion.Entry.Option option : suggestion.getOptions()) {
                values.add(option.getText().string());
            }
        }
        return values;
    }

    private Set<ProductItem> extractCompletionProducts(CompletionSuggestion.Entry suggestion) {
        Set<ProductItem> values = new HashSet<>();
        if (!CollectionUtils.isEmpty(suggestion.getOptions())) {
            final ObjectMapper mapper = new ObjectMapper();
            for (CompletionSuggestion.Entry.Option option : suggestion.getOptions()) {
                values.add(
                        mapper.convertValue((option.getHit().getSourceAsMap().get("productItem")), ProductItem.class));
            }
        }
        return values;
    }

}
