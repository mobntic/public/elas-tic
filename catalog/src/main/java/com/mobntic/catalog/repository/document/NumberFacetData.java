package com.mobntic.catalog.repository.document;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Document(indexName = "#{ catalogConfig.indexName }", type = "NumberFacetData")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class NumberFacetData {

    @Field(type = FieldType.Keyword)
    String name;

    @Field(type = FieldType.Double)
    Double value;

}
