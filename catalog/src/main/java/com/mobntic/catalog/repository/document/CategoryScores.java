package com.mobntic.catalog.repository.document;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import lombok.Builder;
import lombok.Getter;

@Document(indexName = "#{ catalogConfig.indexName }", type = "CategoryScores")
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CategoryScores {

    @Field(type = FieldType.Double)
    private Integer numberOfImpressions = 1;

    @Field(type = FieldType.Double)
    private Integer numberOfOrders = 1;

}