package com.mobntic.catalog.repository.document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.List;
import java.util.Map;

@Document(indexName = "#{ catalogConfig.indexName }", type = "CatalogDocument")
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CatalogDocument {

    @Id
    private String id;

    @Field(type = FieldType.Object, index = false)
    private ProductItem productItem;

    @Field(type = FieldType.Object)
    private SearchData searchData;

    @Field(type = FieldType.Object)
    private CompletionData completionTerms;

    @Field(type = FieldType.Text)
    private List<String> suggestionTerms;

    @Field(type = FieldType.Object)
    private Map<String, Double> numberSort;

    @Field(type = FieldType.Object)
    private Map<String, String> stringSort;

    @Field(type = FieldType.Object)
    private Scores scores;

    @Field(type = FieldType.Object)
    private Category category;

    @Field(type = FieldType.Object)
    private CategoryScores categoryScores;

}
