package com.mobntic.catalog.repository.reponse;

import com.mobntic.catalog.repository.document.ProductItem;
import com.mobntic.catalog.repository.query.CatalogSearchQuery;
import lombok.Getter;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.MultiBucketsAggregation;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static com.mobntic.catalog.repository.query.CatalogSearchQuery.GROUP_BY_FACET_VALUE;

@Getter
public class CatalogSearchQueryResponse {

    private long totalElements;

    private int totalPages;

    private List<ProductItem> products;

    private Map<String, Set<String>> stringFacets = new HashMap<>();

    private Map<String, Set<Double>> numericFacets = new HashMap<>();

    private Pageable pageable;

    private CatalogSearchQueryResponse(AggregatedPage<ProductItem> results) {
        this.totalElements = results.getTotalElements();
        this.totalPages = results.getTotalPages();
        this.products = results.getContent();

        if (results.getAggregations() != null) {
            this.stringFacets = extractStringFacets(
                    ((Nested) results.getAggregations().get(CatalogSearchQuery.AGGS_STRING_FACETS_NAME)).getAggregations());

            this.numericFacets = extractNumericFacets(
                    ((Nested) results.getAggregations().get(CatalogSearchQuery.AGGS_NUMBER_FACETS_NAME)).getAggregations());
        }

        this.pageable = results.getPageable();
    }

    public static CatalogSearchQueryResponse wrap(AggregatedPage<ProductItem> results) {
        return new CatalogSearchQueryResponse(results);
    }

    private Map<String, Set<String>> extractStringFacets(Aggregations aggregations) {

        Map<String, Set<String>> result = new HashMap<>();

        if (Objects.nonNull(aggregations)) {
            result = ((Terms) aggregations.get(CatalogSearchQuery.GROUP_BY_FACET_NAME)).getBuckets().stream().collect(
                    Collectors.toMap(MultiBucketsAggregation.Bucket::getKeyAsString,
                            bucket -> extractStringFacetsValues((StringTerms.Bucket) bucket), (a, b) -> b));
        }

        return result;
    }

    private Map<String, Set<Double>> extractNumericFacets(Aggregations aggregations) {
        Map<String, Set<Double>> result = new HashMap<>();
        if (Objects.nonNull(aggregations)) {
            result = ((Terms) aggregations.get(CatalogSearchQuery.GROUP_BY_FACET_NAME)).getBuckets().stream().collect(
                    Collectors.toMap(MultiBucketsAggregation.Bucket::getKeyAsString, this::extractNumericFacetsValues,
                            (a, b) -> b));
        }

        return result;
    }

    private Set<String> extractStringFacetsValues(StringTerms.Bucket bucket) {

        Set<String> result = new HashSet<>();

        if (Objects.nonNull(bucket) && Objects.nonNull(bucket.getAggregations())) {
            result = ((Terms) bucket.getAggregations().get(GROUP_BY_FACET_VALUE)).getBuckets().stream()
                    .map(Terms.Bucket::getKeyAsString).collect(Collectors.toSet());
        }

        return result;
    }

    private Set<Double> extractNumericFacetsValues(Terms.Bucket bucket) {

        Set<Double> result = new HashSet<>();

        if (Objects.nonNull(bucket) && Objects.nonNull(bucket.getAggregations())) {
            result = ((Terms) bucket.getAggregations().get(GROUP_BY_FACET_VALUE)).getBuckets().stream()
                    .map(value -> (Double) value.getKeyAsNumber()).collect(Collectors.toSet());
        }

        return result;
    }

}
