package com.mobntic.catalog.repository.document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Currency;

@Document(indexName = "#{ catalogConfig.indexName }", type = "Price")
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Price {

    @Field(type = FieldType.Double)
    private Double amount;

    @Field(type = FieldType.Text)
    private Currency currency;

}
