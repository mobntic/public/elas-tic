package com.mobntic.catalog.repository.document;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import lombok.Builder;
import lombok.Getter;

@Document(indexName = "#{ catalogConfig.indexName }", type = "SearchData")
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SearchData {

    @Field(type = FieldType.Text, analyzer = "standard", searchAnalyzer = "standard")
    private String fullText;

    @Field(type = FieldType.Text, analyzer = "standard", searchAnalyzer = "standard")
    private String fullTextBoosted;

    @Field(type = FieldType.Nested, includeInParent = true)
    private List<StringFacetData> stringFacet;

    @Field(type = FieldType.Nested)
    private List<NumberFacetData> numberFacet;
}
