package com.mobntic.catalog.repository;

import com.mobntic.catalog.repository.document.CatalogDocument;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository
        extends ElasticsearchRepository<CatalogDocument, String>, PagingAndSortingRepository<CatalogDocument, String> {

    Page<CatalogDocument> findByProductItem_name(String name, Pageable pageable);

    List<CatalogDocument> findByProductItem_priceBetween(Double minPrice, Double maxPrice);

}
