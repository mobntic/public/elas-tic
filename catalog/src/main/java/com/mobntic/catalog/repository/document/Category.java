package com.mobntic.catalog.repository.document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.ArrayList;
import java.util.List;

@Document(indexName = "#{ catalogConfig.indexName }", type = "Category")
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Category {

    @Field(type = FieldType.Long)
    private Long parentCategoryId;

    @Field(type = FieldType.Text)
    private String name;

    @Field(type = FieldType.Text)
    private List<String> allParents = new ArrayList<>();

    @Field(type = FieldType.Text)
    private List<String> paths = new ArrayList<>();

}
