package com.mobntic.catalog.repository.custom;

import org.elasticsearch.action.search.SearchResponse;

import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;

import com.mobntic.catalog.repository.document.CatalogDocument;
import com.mobntic.catalog.repository.query.CatalogSearchQuery;

public interface CatalogDocumentRepository {

    AggregatedPage<CatalogDocument> searchAndAggregate(Pageable pageable, CatalogSearchQuery query);

    AggregatedPage<CatalogDocument> search(Pageable pageable, CatalogSearchQuery query);

    SearchResponse suggestCompletion(CatalogSearchQuery query);
}
