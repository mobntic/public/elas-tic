package com.mobntic.catalog.repository.document;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.*;
import org.springframework.data.elasticsearch.annotations.CompletionField;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.completion.Completion;

@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Document(indexName = "#{ catalogConfig.indexName }", type = "CompletionData")
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CompletionData {

    @CompletionField
    private Completion product;

    @CompletionField
    private Completion category;

    @CompletionField
    private Completion manufacturer;

    @CompletionField
    private Completion seller;
}