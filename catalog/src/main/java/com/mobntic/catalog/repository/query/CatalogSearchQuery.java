package com.mobntic.catalog.repository.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.apache.lucene.search.join.ScoreMode;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.NestedQueryBuilder;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.suggest.SuggestBuilder;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;

import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import static java.util.Optional.empty;
import static java.util.Optional.of;
import static org.apache.logging.log4j.util.Strings.isNotBlank;
import static org.elasticsearch.action.search.SearchType.DEFAULT;
import static org.elasticsearch.index.query.Operator.AND;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.nestedQuery;
import static org.elasticsearch.index.query.QueryBuilders.termQuery;
import static org.elasticsearch.index.query.QueryBuilders.termsQuery;
import static org.elasticsearch.search.aggregations.AggregationBuilders.terms;
import static org.elasticsearch.search.suggest.SuggestBuilders.completionSuggestion;

@Getter
@AllArgsConstructor
@Builder
public class CatalogSearchQuery {

    public static final String FACET_NAME = ".name";
    public static final String FACET_VALUE = ".value";
    public static final String AGGS_STRING_FACETS_NAME = "string_facets";
    public static final String NESTED_SEARCH_DATA_STRING_FACET_PATH = "searchData.stringFacet";
    public static final String AGGS_NUMBER_FACETS_NAME = "number_facets";
    public static final String NESTED_SEARCH_DATA_NUMBER_FACET_PATH = "searchData.numberFacet";
    public static final String GROUP_BY_FACET_NAME = "group_by_facet_name";
    public static final String GROUP_BY_FACET_VALUE = "group_by_facet_value";
    private int page = 0;
    private int pageSize = 10;
    private int startIndex = 0;

    private String query;
    private String parentCategory;

    private Map<String, String[]> stringFacets;

    private Map<String, Double[]> numberFacets;

    private String sortQuery;

    private Long categoryId;
    private boolean searchExplicitCategory = false;

    private PageRequest pageRequest;

    public NativeSearchQueryBuilder queryBuilder() {

        NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder().withSearchType(DEFAULT);

        pageRequest = PageRequest.of(page, pageSize);
        searchQueryBuilder.withPageable(pageRequest);

        if (isNotBlank(query)) {
            searchQueryBuilder
                    .withQuery(
                            matchQuery("searchData.fullText", query)
                                    .operator(AND));
        }

        if (isNotBlank(parentCategory)) {
            searchQueryBuilder
                    .withQuery(
                            matchQuery("category.name", parentCategory)
                                    .operator(AND));
        }

        if (Objects.nonNull(categoryId)) {
            searchQueryBuilder
                    .withQuery(
                            matchQuery("category.parentCategoryId", categoryId)
                                    .operator(AND)
                    );
        }

        return searchQueryBuilder;
    }

    public NativeSearchQueryBuilder queryAndGroupBuilder() {
        NativeSearchQueryBuilder searchQueryBuilder = queryBuilder();

        searchQueryBuilder
                .addAggregation(
                        getFacetAggregationBuilder(AGGS_STRING_FACETS_NAME, NESTED_SEARCH_DATA_STRING_FACET_PATH))
                .addAggregation(
                        getFacetAggregationBuilder(AGGS_NUMBER_FACETS_NAME, NESTED_SEARCH_DATA_NUMBER_FACET_PATH));

        Optional<NestedQueryBuilder> strFilters = getNestedFacet(NESTED_SEARCH_DATA_STRING_FACET_PATH, stringFacets);


        strFilters.ifPresent(searchQueryBuilder::withFilter);

        Optional<NestedQueryBuilder> nbrFilters = getNestedFacet(NESTED_SEARCH_DATA_NUMBER_FACET_PATH, numberFacets);
        if (nbrFilters.isPresent()) {
            searchQueryBuilder
                    .withFilter(
                            nbrFilters.get()
                    );
        }

        return searchQueryBuilder;
    }

    public SuggestBuilder suggestCompletion() {
        return new SuggestBuilder()
                .addSuggestion(
                        "completion-product",
                        completionSuggestion("completionTerms.product").text(query)
                ).addSuggestion(
                        "completion-category",
                        completionSuggestion("completionTerms.category").text(query)
                ).addSuggestion(
                        "completion-manufacturer",
                        completionSuggestion("completionTerms.manufacturer").text(query)
                ).addSuggestion(
                        "completion-seller",
                        completionSuggestion("completionTerms.seller").text(query)
                );
    }

    private Optional<NestedQueryBuilder> getNestedFacet(String path, Map<String, ?> facets) {
        if (facets != null && !facets.isEmpty()) {
            BoolQueryBuilder facetFilters = boolQuery();
            for (Map.Entry<String, ?> facet : facets.entrySet()) {
                facetFilters
                        .must(
                                getFacetFilterTermQuery(path, facet.getKey(), (Object[]) facet.getValue())
                        );
            }

            return of(nestedQuery(path, facetFilters, ScoreMode.None));
        }
        return empty();
    }

    private BoolQueryBuilder getFacetFilterTermQuery(String path, String key, Object[] values) {
        return boolQuery()
                .must(
                        termQuery(path + FACET_NAME, key))
                .must(
                        termsQuery(path + FACET_VALUE, values)
                );
    }

    private AbstractAggregationBuilder<?> getFacetAggregationBuilder(String name, String path) {
        return AggregationBuilders
                .nested(name, path)
                .subAggregation(
                        terms(GROUP_BY_FACET_NAME)
                                .field(path + FACET_NAME)
                                .subAggregation(
                                        terms(GROUP_BY_FACET_VALUE)
                                                .field(path + FACET_VALUE)
                                )
                );
    }
}
