package com.mobntic.catalog.repository.document;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import lombok.Builder;
import lombok.Getter;

@Document(indexName = "#{ catalogConfig.indexName }", type = "SkuItem")
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SkuItem {

    @Id
    private String skuId;

    @Field(type = FieldType.Text)
    private String name;

    @Field(type = FieldType.Double)
    private Double price;

    @Field(type = FieldType.Text)
    private String image;

}
