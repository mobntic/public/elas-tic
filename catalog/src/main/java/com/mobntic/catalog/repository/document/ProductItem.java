package com.mobntic.catalog.repository.document;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import lombok.Builder;
import lombok.Getter;

@Document(indexName = "#{ catalogConfig.indexName }", type = "ProductItem")
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProductItem {

    @Id
    private String id;

    @Field(type = FieldType.Text)
    private String name;

    @Field(type = FieldType.Text)
    private String description;

    @Field(type = FieldType.Double)
    private Double price;

    @Field(type = FieldType.Nested)
    private List<SkuItem> skus;

    @Field(type = FieldType.Boolean)
    private Boolean active;

    @Field(type = FieldType.Date)
    private Date activeStartDate;

    @Field(type = FieldType.Date)
    private Date activeEndDate;

    @Field(type = FieldType.Text)
    private String notification;

    @Field(type = FieldType.Text)
    private String image;

    @Field(type = FieldType.Text)
    private String brand;

    @Field(type = FieldType.Text)
    private String inventoryType;

    @Field(type = FieldType.Nested)
    private Price retailPrice;

    @Field(type = FieldType.Nested)
    private Price salePrice;

}
