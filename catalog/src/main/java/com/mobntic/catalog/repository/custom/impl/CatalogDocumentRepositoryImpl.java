package com.mobntic.catalog.repository.custom.impl;

import com.mobntic.catalog.repository.custom.CatalogDocumentRepository;
import com.mobntic.catalog.repository.document.CatalogDocument;
import com.mobntic.catalog.repository.query.CatalogSearchQuery;
import lombok.extern.log4j.Log4j2;
import org.elasticsearch.action.search.SearchResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Repository;

import static com.mobntic.catalog.tools.JsonElasticObjectMapper.logDebug;

@Repository
@Log4j2
public class CatalogDocumentRepositoryImpl implements CatalogDocumentRepository {

    ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    public void setElasticsearchTemplate(ElasticsearchTemplate elasticsearchTemplate) {
        this.elasticsearchTemplate = elasticsearchTemplate;
    }

    @Override
    public AggregatedPage<CatalogDocument> searchAndAggregate(Pageable pageable, CatalogSearchQuery query) {

        SearchQuery searchQuery = query.queryAndGroupBuilder().build();

        logDebug(log, searchQuery);

        return elasticsearchTemplate.queryForPage(searchQuery, CatalogDocument.class);
    }

    @Override
    public AggregatedPage<CatalogDocument> search(Pageable pageable, CatalogSearchQuery query) {

        SearchQuery searchQuery = query.queryBuilder().build();

        logDebug(log, searchQuery);

        return elasticsearchTemplate.queryForPage(searchQuery, CatalogDocument.class);

    }

    @Override
    public SearchResponse suggestCompletion(CatalogSearchQuery query) {
        return elasticsearchTemplate.suggest(query.suggestCompletion(), CatalogDocument.class);
    }

}
