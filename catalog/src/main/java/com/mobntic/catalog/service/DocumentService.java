package com.mobntic.catalog.service;

import com.mobntic.catalog.repository.DocumentRepository;
import com.mobntic.catalog.repository.custom.CatalogDocumentRepository;
import com.mobntic.catalog.repository.document.CatalogDocument;
import com.mobntic.catalog.repository.document.ProductItem;
import com.mobntic.catalog.repository.query.CatalogSearchQuery;
import com.mobntic.catalog.repository.reponse.CatalogSearchQueryResponse;
import com.mobntic.catalog.repository.reponse.CatalogSuggestCompletionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.aggregation.impl.AggregatedPageImpl;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
public class DocumentService {

    private DocumentRepository documentRepository;

    private CatalogDocumentRepository catalogDocumentRepository;

    @Autowired(required = false)
    public void setDocumentRepository(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    @Autowired(required = false)
    public void setCatalogDocumentRepository(CatalogDocumentRepository catalogDocumentRepository) {
        this.catalogDocumentRepository = catalogDocumentRepository;
    }

    public CatalogDocument save(CatalogDocument catalogDocument) {
        return documentRepository.save(catalogDocument);
    }

    public void delete(CatalogDocument catalogDocument) {
        documentRepository.delete(catalogDocument);
    }

    public Optional<CatalogDocument> findById(String id) {
        return documentRepository.findById(id);
    }

    public Iterable<CatalogDocument> findAll() {
        return documentRepository.findAll();
    }

    public Page<CatalogDocument> findByName(String name, PageRequest pageRequest) {
        return documentRepository.findByProductItem_name(name, pageRequest);
    }

    public List<ProductItem> findByPriceBetween(double minPrice, double maxPrice) {
        return documentRepository.findByProductItem_priceBetween(minPrice, maxPrice).stream()
                .map(CatalogDocument::getProductItem).collect(toList());
    }

    public CatalogSearchQueryResponse findByQuery(CatalogSearchQuery searchQuery) {
        AggregatedPage<CatalogDocument> result = catalogDocumentRepository
                .search(searchQuery.getPageRequest(), searchQuery);
        return transformCatalogDocument(result);
    }

    public CatalogSearchQueryResponse findAndGroupByQuery(CatalogSearchQuery searchQuery) {
        AggregatedPage<CatalogDocument> result = catalogDocumentRepository
                .searchAndAggregate(searchQuery.getPageRequest(), searchQuery);
        return transformCatalogDocument(result);
    }

    public CatalogSuggestCompletionResponse suggestCompletion(CatalogSearchQuery query) {
        return CatalogSuggestCompletionResponse.wrap(catalogDocumentRepository.suggestCompletion(query));
    }

    public CatalogSearchQueryResponse transformCatalogDocument(AggregatedPage<CatalogDocument> result) {
        return CatalogSearchQueryResponse.wrap(new AggregatedPageImpl<>(
                result.getContent().stream().map(CatalogDocument::getProductItem).collect(toList()),
                result.getPageable(), result.getTotalElements(), result.getAggregations(), result.getScrollId()));
    }
}
