package com.mobntic.catalog.tools;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.apache.logging.log4j.Logger;


public class JsonElasticObjectMapper {

    private static ObjectMapper mapper = new ObjectMapper().configure(SerializationFeature.INDENT_OUTPUT, true);

    public static void logDebug(Logger log, Object obj){
        log.debug("-------------------------------------------------------------------");
        log.debug("|             " + obj.getClass().getName());
        log.debug("-------------------------------------------------------------------");
        try {
            log.debug(mapper.writeValueAsString(obj));
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
        log.debug("-------------------------------------------------------------------");
    }
}
