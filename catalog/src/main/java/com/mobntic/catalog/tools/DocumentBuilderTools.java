package com.mobntic.catalog.tools;

import com.mobntic.catalog.repository.document.CatalogDocument;
import com.mobntic.catalog.repository.document.Category;
import com.mobntic.catalog.repository.document.CategoryScores;
import com.mobntic.catalog.repository.document.CompletionData;
import com.mobntic.catalog.repository.document.NumberFacetData;
import com.mobntic.catalog.repository.document.Price;
import com.mobntic.catalog.repository.document.ProductItem;
import com.mobntic.catalog.repository.document.Scores;
import com.mobntic.catalog.repository.document.SearchData;
import com.mobntic.catalog.repository.document.SkuItem;
import com.mobntic.catalog.repository.document.StringFacetData;
import org.springframework.data.elasticsearch.core.completion.Completion;

import java.util.Arrays;
import java.util.Collections;
import java.util.Currency;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.lang.String.format;
import static java.util.Collections.emptyList;
import static java.util.Currency.getInstance;
import static java.util.Optional.of;

public class DocumentBuilderTools {

    private DocumentBuilderTools() {
    }

    public static CatalogDocument.CatalogDocumentBuilder buildCatalogDocument(String productId, ProductItem productItem,
                                                                              SearchData searchData, CompletionData completionTerms, Scores scores, Category category,
                                                                              CategoryScores categoryScores) {
        return CatalogDocument.builder()
                .id(
                        productId
                )
                .productItem(
                        productItem
                )
                .searchData(
                        searchData
                )
                .completionTerms(
                        completionTerms
                )
                .suggestionTerms(
                        extractSuggestionTerms()
                )
                .numberSort(
                        extractNumberSort()
                )
                .stringSort(
                        extractStringSort()
                )
                .scores(
                        scores
                )
                .category(
                        category
                )
                .categoryScores(
                        categoryScores
                );
    }

    public static CatalogDocument.CatalogDocumentBuilder builCatalogDocument(String productId, String skuId,
                                                                             String name, String desc, Double price,
                                                                             Long categoryId, String categoryName) {

        return buildCatalogDocument(
                productId,
                buildProductItem(
                        productId,
                        name,
                        desc,
                        price,
                        Arrays.asList(
                                buildSkuItem(skuId, name + "#1", price, "image_key").build(),
                                buildSkuItem(skuId, name + "#2", price, "image_key").build()
                        ),
                        "image_key",
                        "Brand"
                ).build(),
                buildSearchData(name, 1).build(),
                extractCompletionTerms(emptyList(), emptyList(), emptyList(), emptyList())
                        .build(),
                buildScores()
                        .build(),
                buildCategory(categoryId, categoryName)
                        .build(),
                buildCategoryScores()
                        .build());
    }

    private static Map<String, Double> extractNumberSort() {
        Map<String, Double> data = new HashMap<>();
        data.put("final_gross_price", 123d);
        return data;
    }

    private static Map<String, String> extractStringSort() {
        Map<String, String> data = new HashMap<>();
        data.put("name", "product");
        return data;
    }


    public static Price.PriceBuilder buildPrice(Double price, Optional<Currency> currency) {
        return Price.builder()
                .amount(price)
                .currency(currency.orElse(getInstance("DZD")));
    }

    public static ProductItem.ProductItemBuilder buildProductItem(String productId, String name, String desc,
                                                                  Double price, List<SkuItem> skuItems, String imageKey, String brand) {
        return ProductItem.builder()
                .id(productId)
                .name(name)
                .description(desc)
                .price(price)
                .skus(skuItems)
                .image(imageKey)
                .brand(brand)
                .retailPrice(
                        buildPrice(price, of(getInstance("DZD")))
                                .build()
                )
                .salePrice(
                        buildPrice(price, of(getInstance("DZD")))
                                .build()
                );
    }

    public static SkuItem.SkuItemBuilder buildSkuItem(String skuId, String name, Double price, String image) {
        return SkuItem.builder()
                .skuId(skuId)
                .name(name)
                .price(price)
                .image(image);
    }

    public static SearchData.SearchDataBuilder buildSearchData(String fullText, String boostedText,
                                                               List<StringFacetData> stringFacet, List<NumberFacetData> numberFacet) {
        return SearchData.builder()
                .fullText(fullText)
                .fullTextBoosted(boostedText)
                .stringFacet(stringFacet)
                .numberFacet(numberFacet);
    }

    public static SearchData.SearchDataBuilder buildSearchData(String name, int i) {

        return buildSearchData(
                format("Neque porro{%d} est qui dolorem{%d} consectetur{%d}, adipisci velit...", i, i, i),
                format(" porro{%d} est qui dolorem{%d} consectetur{%d}", i, i, i), Arrays.asList(
                        new StringFacetData("manufacturer", "Dell_" + name + "_" + i),
                        new StringFacetData("format", "Grand_" + name + "_" + i)
                ), Collections.singletonList(
                        new NumberFacetData("final_gross_price", 12d * i)
                ));
    }

    public static CompletionData.CompletionDataBuilder extractCompletionTerms(List<String> categories,
                                                                              List<String> nameSuggestion, List<String> manufacturer, List<String> sellers) {

        CompletionData.CompletionDataBuilder completionData = CompletionData.builder();

        completionData.category(new Completion(categories.toArray(new String[categories.size()])))
                .manufacturer(new Completion(manufacturer.toArray(new String[manufacturer.size()])))
                .product(new Completion(nameSuggestion.toArray(new String[nameSuggestion.size()])))
                .seller(new Completion(sellers.toArray(new String[sellers.size()])));

        return completionData;
    }

    public static List<String> extractSuggestionTerms() {
        return Arrays.asList(
                "Suggestion_1",
                "Suggestion_2"
        );
    }

    public static Scores.ScoresBuilder buildScores() {
        return Scores.builder();
    }

    public static Category.CategoryBuilder buildCategory(Long categoryId, String categoryName) {
        return Category.builder()
                .name(categoryName)
                .parentCategoryId(categoryId);
    }

    public static CategoryScores.CategoryScoresBuilder buildCategoryScores() {
        return CategoryScores.builder();
    }
}
