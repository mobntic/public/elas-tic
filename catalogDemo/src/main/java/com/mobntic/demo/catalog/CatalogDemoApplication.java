package com.mobntic.demo.catalog;

import com.mobntic.catalog.repository.document.CatalogDocument;
import com.mobntic.catalog.repository.document.NumberFacetData;
import com.mobntic.catalog.repository.document.StringFacetData;
import com.mobntic.catalog.service.DocumentService;
import lombok.Setter;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.mobntic.catalog.tools.DocumentBuilderTools.buildCatalogDocument;
import static com.mobntic.catalog.tools.DocumentBuilderTools.buildCategory;
import static com.mobntic.catalog.tools.DocumentBuilderTools.buildCategoryScores;
import static com.mobntic.catalog.tools.DocumentBuilderTools.buildProductItem;
import static com.mobntic.catalog.tools.DocumentBuilderTools.buildScores;
import static com.mobntic.catalog.tools.DocumentBuilderTools.buildSearchData;
import static com.mobntic.catalog.tools.DocumentBuilderTools.buildSkuItem;
import static com.mobntic.catalog.tools.DocumentBuilderTools.extractCompletionTerms;
import static java.util.Arrays.asList;

@Log4j2
@SpringBootApplication
@ComponentScan({"com.mobntic"})
@Setter(onMethod = @__({@Autowired}))
public class CatalogDemoApplication {

    private DocumentService documentService;

    public static void main(String[] args) {
        SpringApplication.run(CatalogDemoApplication.class, args);
    }

    @Bean
    InitializingBean initCatalog() {

        return () -> {
            for (int i = 0; i < 58; i++) {

                String productId = UUID.randomUUID().toString();
                String skuId = UUID.randomUUID().toString();

                String name = randomSentence(3, 6);
                String desc = randomSentence(40, 2);

                Double price = 15 + (new Random().nextDouble() * (99 - 15));
                String brand = randomSentence(1, 3);

                CatalogDocument doc = documentService.save(buildDocument(productId, skuId, name, desc, price,
                        brand, "Category_" + (i / 6),
                        randomFacet(brand), randomFacetNumber(price))
                );
                log.info(String.format("[%d] %s - %s - %s", i, doc.getId(), doc.getProductItem().getName(), doc.getProductItem().getBrand()));

            }
        };
    }

    private Map<String, Double> randomFacetNumber(final Double price) {
        Map<String, Double> result = new HashMap<>();
        result.put("Price", price);
        result.put("Garantie", (double) new Random().nextInt(3));
        return result;
    }

    private Map<String, String> randomFacet(final String brand) {
        Map<String, String> result = new HashMap<>();
        result.put("Marque", brand);
        result.put("Option", "Option_" + new Random().nextInt(3));
        return result;
    }

    private CatalogDocument buildDocument(final String productId,
                                          final String skuId, final String name,
                                          final String desc, final Double price,
                                          final String brand, final String category,
                                          final Map<String, String> facets,
                                          final Map<String, Double> numberFacets) {
        return buildCatalogDocument(productId,
                buildProductItem(
                        productId,
                        name,
                        desc,
                        price,
                        Collections.singletonList(
                                buildSkuItem(
                                        skuId,
                                        name,
                                        price,
                                        UUID.randomUUID().toString()
                                ).build()
                        ),
                        UUID.randomUUID().toString(),
                        brand
                ).active(new Random().nextInt(3) % 2 == 0)
                        .build(),
                buildSearchData(getFullText(skuId, name, brand, category),
                        "",
                        facets.entrySet().stream()
                                .map(facet -> new StringFacetData(
                                        facet.getKey(),
                                        facet.getValue())
                                ).collect(Collectors.toList())
                        , numberFacets.entrySet().stream()
                                .map(facet -> new NumberFacetData(
                                        facet.getKey(),
                                        facet.getValue())
                                ).collect(Collectors.toList())
                ).build(),
                extractCompletionTerms(
                        Collections.singletonList(category),
                        asList(name.split(" ")),
                        Collections.singletonList(brand),
                        Collections.singletonList("Seller")
                ).build(),
                buildScores().build(),
                buildCategory(UUID.randomUUID().getMostSignificantBits(), category).build(),
                buildCategoryScores().build()
        ).build();
    }

    private String getFullText(final String skuId, final String name, final String brand, final String category) {

        StringBuilder result = new StringBuilder();
        result.append(skuId);
        result.append(",");
        Arrays.stream(name.split(" ")).map(n -> n + ",").collect(Collectors.toList()).forEach(result::append);
        result.append(brand);
        result.append(",");
        result.append(category);

        return result.toString();
    }

    private String randomSentence(int nbrWords, int minLength) {

        StringBuilder sentence = new StringBuilder();
        for (int i = 0; i < nbrWords; i++) {
            sentence.append(RandomStringUtils.randomAlphabetic(new Random().nextInt(20) + minLength));
            sentence.append(" ");
        }

        return sentence.toString();
    }

}
