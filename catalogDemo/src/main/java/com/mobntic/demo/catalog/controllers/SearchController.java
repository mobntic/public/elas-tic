package com.mobntic.demo.catalog.controllers;

import com.mobntic.catalog.repository.document.CatalogDocument;
import com.mobntic.catalog.repository.document.ProductItem;
import com.mobntic.catalog.repository.query.CatalogSearchQuery;
import com.mobntic.catalog.repository.reponse.CatalogSearchQueryResponse;
import com.mobntic.catalog.repository.reponse.CatalogSuggestCompletionResponse;
import com.mobntic.catalog.service.DocumentService;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.util.Arrays.asList;

@RestController
@Setter(onMethod = @__({@Autowired}))
public class SearchController {

    private static final String QUERY_PARAM = "q";
    private static final String CATEGORY_PARAM = "c";
    private static final String PAGE_PARAM = "page";
    private static final String PAGE_SIZE_PARAM = "pageSize";
    public static final String MIN_PARAM = "min";
    public static final String MAX_PARAM = "max";
    public static final String PAGE_DEFAULT_VALUE = "0";
    public static final String PAGE_SIZE_DEFAULT_VALUE = "10";
    public static final String EMPTY_VALUE = "";
    public static final String SUGGESTION_PARAM = "s";

    private DocumentService documentService;

    @GetMapping(value = "/search", params = "debug")
    @ResponseStatus(HttpStatus.OK)
    public Iterable<CatalogDocument> getAllProducts() {
        return documentService.findAll();
    }

    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public CatalogSearchQueryResponse retrieveAllProducts(
            @RequestParam(value = CATEGORY_PARAM, defaultValue = EMPTY_VALUE, required = false) String category,
            @RequestParam(value = PAGE_PARAM, defaultValue = PAGE_DEFAULT_VALUE, required = false) int page,
            @RequestParam(value = PAGE_SIZE_PARAM, defaultValue = PAGE_SIZE_DEFAULT_VALUE, required = false) int pageSize) {

        CatalogSearchQuery.CatalogSearchQueryBuilder query = CatalogSearchQuery.builder()
                .page(page)
                .pageSize(pageSize);

        if (!StringUtils.isEmpty(category)) {
            query.parentCategory(category);
        }

        return documentService.findAndGroupByQuery(query.build());
    }

    @GetMapping(value = "/search", params = QUERY_PARAM)
    @ResponseStatus(HttpStatus.OK)
    public CatalogSearchQueryResponse searchQuery(@RequestParam(QUERY_PARAM) String query,
                                                  @RequestParam(value = CATEGORY_PARAM, defaultValue = EMPTY_VALUE, required = false) String category,
                                                  @RequestParam(value = PAGE_PARAM, defaultValue = PAGE_DEFAULT_VALUE, required = false) int page,
                                                  @RequestParam(value = PAGE_SIZE_PARAM, defaultValue = PAGE_SIZE_DEFAULT_VALUE, required = false) int pageSize) {

        CatalogSearchQuery.CatalogSearchQueryBuilder queryBuilder = CatalogSearchQuery.builder()
                .query(query)
                .page(page)
                .pageSize(pageSize);

        if (!StringUtils.isEmpty(category)) {
            queryBuilder.parentCategory(category);
        }

        return documentService.findAndGroupByQuery(queryBuilder.build());
    }

    @GetMapping(value = "/search/facets")
    @ResponseStatus(HttpStatus.OK)
    public CatalogSearchQueryResponse searchFacet(@RequestParam Map<String, String> params) {

        String category = params.getOrDefault(CATEGORY_PARAM, EMPTY_VALUE);
        params.remove(CATEGORY_PARAM);
        int page = Integer.valueOf(params.getOrDefault(PAGE_PARAM, PAGE_DEFAULT_VALUE));
        params.remove(PAGE_PARAM);
        int pageSize = Integer.valueOf(params.getOrDefault(PAGE_SIZE_PARAM, PAGE_SIZE_DEFAULT_VALUE));
        params.remove(PAGE_SIZE_PARAM);

        CatalogSearchQuery.CatalogSearchQueryBuilder queryBuilder = CatalogSearchQuery.builder()
                .stringFacets(transformParamsMap(params))
                .page(page)
                .pageSize(pageSize);

        if (!StringUtils.isEmpty(category)) {
            queryBuilder.parentCategory(category);
        }

        return documentService.findAndGroupByQuery(queryBuilder.build());
    }

    private Map<String, String[]> transformParamsMap(final Map<String, String> params) {

        Map<String, String[]> result = new HashMap<>();
        if(!params.isEmpty()){
            params.forEach((key, value) -> result.put(key, Collections.singletonList(value).toArray(new String[0])));
        }
        return result;
    }

    @GetMapping(value = "/search", params = SUGGESTION_PARAM)
    @ResponseStatus(HttpStatus.OK)
    public CatalogSuggestCompletionResponse suggestCompletion(@RequestParam(SUGGESTION_PARAM) String query,
                                                              @RequestParam(value = PAGE_PARAM, defaultValue = PAGE_DEFAULT_VALUE, required = false) int page,
                                                              @RequestParam(value = PAGE_SIZE_PARAM, defaultValue = PAGE_SIZE_DEFAULT_VALUE, required = false) int pageSize) {

        return documentService.suggestCompletion(CatalogSearchQuery.builder()
                .query(query)
                .build());
    }

    @GetMapping(value = "/search/price")
    @ResponseStatus(HttpStatus.OK)
    public List<ProductItem> searchPriceInterval(@RequestParam(MIN_PARAM) double minPrice,
                                                 @RequestParam(MAX_PARAM) double maxPrice,
                                                 @RequestParam(value = PAGE_PARAM, defaultValue = PAGE_DEFAULT_VALUE, required = false) int page,
                                                 @RequestParam(value = PAGE_SIZE_PARAM, defaultValue = PAGE_SIZE_DEFAULT_VALUE, required = false) int pageSize) {

        return documentService.findByPriceBetween(minPrice, maxPrice);
    }

}
